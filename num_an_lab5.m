f=@(x) cos(77*asin(0.3*sqrt((x+4)/7.7)));

lp = -2; 
rp = 2;
N = 25;
step = (rp-lp)/(N-1); 
x=lp:step:rp;

sc=2*pi/(rp-lp);

basis = {@(x) 1+x-x,...
        @(x) sin(x),...
        @(x) cos(x),...
        @(x) sin(2*x),...
        @(x) cos(2*x),...
        };

m=length(basis);
coefs = zeros(1,m);

P=zeros(m,m);
for i=1:m
    for j=1:m
        P(i,j)=dot(basis{j}(x),basis{i}(x));
    end
end

fp = zeros(1,m);
for i=1:m
    fp(i)=dot(f(x),basis{i}(x));
end

coefs = P\fp';


figure(1);
fplot(f,[lp rp]);
hold on;

fplot(@(x) lsmi(x,coefs,basis),[lp rp]);

legend('original function', 'LSMI');
grid
hold off

err=sum((lsmi(x,coefs,basis)-f(x)).^2);
disp(err);

function res = lsmi(x,coefs,basis)
    res=0;
    m=length(basis);
    for i=1:m
        res=res+coefs(i)*basis{i}(x);
    end
end
